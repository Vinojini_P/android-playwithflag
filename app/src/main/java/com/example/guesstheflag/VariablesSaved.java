package com.example.guesstheflag;

public class VariablesSaved {
    String name;
    int images;

    public VariablesSaved(String name, int images) {
        this.name = name;
        this.images = images;
    }
    public VariablesSaved(){

    }

    public String getName() {
        return name;
    }

    public int getImages() {
        return images;
    }
}
